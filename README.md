# plantuml-icon-font-sprites 

#### 介绍
* 这是plantuml-icon-font-sprites库的国内同步镜像，目前暂时1天1更，如果断了请提issue
* 原始仓库地址 https://github.com/tupadr3/plantuml-icon-font-sprites
* 引用例子：
```
!define DEVICONS https://gitee.com/coder4/plantuml-icon-font-sprites/raw/master/mirror/devicons
!include DEVICONS/java.puml

!define FA5 https://gitee.com/coder4/plantuml-icon-font-sprites/raw/master/mirror/font-awesome-5
!include FA5/weixin.puml
```
