#!/bin/bash

# rebase
git pull --rebase

# sync code & modify
rm -rf ./mirror
git clone https://github.com/tupadr3/plantuml-icon-font-sprites ./mirror
rm -rf ./mirror/.git
rm -rf ./mirror/.github

# push
git add . 
git commit -m "sync mirror $(date '+%Y-%m-%d')"
git push origin master

